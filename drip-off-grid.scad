// clang-format off
include <utils/utils.scad>;
// clang-format on

/* [🍽 Plate] */
plate_shape = "⬛ square"; // ["⬛ square"]
plate_thickness = 1.5;
plate_size = [ 80, 150 ]; // [5:0.1:200]
plate_edge_radius = 10;   // [0:0.1:50]

/* [💧 Holes] */
hole_shape = "⬛ square"; // ["⬛ square"]
hole_size = [ 5, 30 ];   // [1:0.1:50]
hole_edge_radius = 1;    // [0:0.1:50]
hole_counts = [ 7, 4 ];  // [1:1:20]
hole_distance = 5;       // [1:0.1:50]

/* [🔩 Pins] */
pin_height = 5;
pin_thickness = 1.5;
pin_skip = 0; // [0:1:10]

epsilon = 0.1 * 1;
$fs = $preview ? 3 : 0.5;
$fa = $preview ? 5 : 1;

module
plate_shape()
{
  edge_radius = min(min(plate_size) / 3, plate_edge_radius);
  offset(edge_radius) offset(-edge_radius) if (plate_shape == "⬛ square")
  {
    square(plate_size, center = true);
  }
}

module
hole_shape()
{
  edge_radius = min(min(hole_size) / 3, hole_edge_radius);
  offset(edge_radius) offset(-edge_radius) if (hole_shape == "⬛ square")
  {
    square(hole_size, center = true);
  }
}

// Plate
linear_extrude(plate_thickness) difference()
{
  plate_shape();
  mosaic(ix = [-hole_counts[0] + 1:2:hole_counts[0] - 1],
         iy = [-hole_counts[1] + 1:2:hole_counts[1] - 1],
         position = function(i, j, d)[i / 2 * (hole_size[0] + hole_distance),
                                      j / 2 * (hole_size[1] + hole_distance)])
    hole_shape();
}

// Pins
linear_extrude(pin_height)
  mosaic(ix = [for (i = [-hole_counts[0]:2:hole_counts[0]]) i],
         iy = [-hole_counts[1] + 1:2:hole_counts[1] - 1],
         position = function(i, j, d)[i / 2 * (hole_size[0] + hole_distance),
                                      j / 2 * (hole_size[1] + hole_distance)],
         decider = function(i, j, d)(hole_counts[0] - abs(i)) %
                     (2 * (pin_skip + 1)) ==
                   0)
{
  pin_size = [
    min(pin_thickness, hole_distance / 2),
    max(pin_thickness, hole_size[1] / 2)
  ];
  pin_edge_radius = min(pin_size) / 3;
  offset(pin_edge_radius) offset(-pin_edge_radius)
    square(pin_size, center = true);
}
